# AdvancedSoftwareProgrammA1
Assignement 1 Maxence Vanderkelen
The model presented is that of an NTNU page for economics studies.
The courses, the department and the professors are the classes used in this model

Economics.xmi is the body representing the economics curriculum through the second year.
The model consists of courses, people and departments

Departments have a unique name, courses and teachers.

Persons have a mandatory first name, they can have a name and courses.
The full name (volatile) was intended to be generated through the firstname and last name but it is not implemented.

Courses have labels, a unique teacher code, a credit, a teaching period and a study year.


![image](https://user-images.githubusercontent.com/99428112/192163129-1d56415c-7a86-493a-997f-e1ec628b6f86.png)



Courses can be represented by the learning period (year and season), the credits they are worth, their titles and their code


![image](https://user-images.githubusercontent.com/99428112/192161099-0aee3ee4-6eb9-48fe-a5b7-198f37524765.png)


Then we have the instance Sok1000 that represents a course, with its name, its credits, its teachers, its code, its credits, the teaching period and the study year.


![image](https://user-images.githubusercontent.com/99428112/192162251-020a93d2-5430-47aa-927f-5a570d1847ee.png)




